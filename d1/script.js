console.log("hello");

/*
What is a Class?
- A class is a blueprint that describes an object in a non-specific way. Class will not define a property, it will only declare what is the needed property.

Why use Classes?
- We saw how repetitive it is to define objects each time we needed a new student. Using a class will make our code reusable.
*/

/*
In JS, we define a class by using the key word "class" and {}.
naming convention for classes: begin with uppercase letters (pascal casing).

Syntax:
	class <Name> {
		
	}
*/


//Student Class
class Student {
/*
to enable students instantiated from this class to have distinct names and emails
Our constructor must be able to accept name, email, and grades, arguments
which it will then use to set the value of the object's correcponding properties
*/
	constructor(name, email, grades){
		this.name = name;
		this.email = email;
		this.grades = grades;
	};
	//'this.name' will come from the parameters

	//Class Methods: These are common to all instances
	login(){
		console.log(`${this.email} has logged in`);
		return this;
	};
	logout(){
		console.log(`${this.email} has logged off`);
		return this;
	}
	listGrades() {
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		return this;
	};
	computeAve() {
		let sum = 0;
		this.gradeAve.forEach(grades => sum += grades);
		this.gradeAve = sum / 4;
		return this;
	};
	willPass() {
		this.passed = this.computeAve().gradeAve >= 85;
		return this;
	};
	willPassWithHonors() {
		if (this.passed) {
			this.passedWithHonors = this.gradeAve >= 90;
		} else {
			this.passedWithHonors = false;
		}
		return this;
	}
}

//Instances
let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
studentOne.login();
studentOne.logout();
studentOne.listGrades();
studentOne.computeAve();
studentOne.willPass();
studentOne.willPassWithHonors();

let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
studentTwo.login();
studentTwo.logout();
studentTwo.listGrades();
studentTwo.computeAve();
studentTwo.willPass();
studentTwo.willPassWithHonors();

let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
studentThree.login();
studentThree.logout();
studentThree.listGrades();
studentThree.computeAve();
studentThree.willPass();
studentThree.willPassWithHonors();

let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);
studentFour.login();
studentFour.logout();
studentFour.listGrades();
studentFour.computeAve();
studentFour.willPass();
studentFour.willPassWithHonors();

/*
The login() method logs a message to the console indicating that the student with the specific email has logged in.

The logout() method logsa message to the console indicating that the student with the specific email has logged out.

The listGrades() method logs a message to the console displaying the name of the student and their quarterly grade averages.

The computeAve() method calculates the average of the student's grades by iterating over the this.grades array and summing up all the grades.The result is stored in the this.gradeAve property.

The willPass() method determines if the student passed or failed based on their average grade. It calls the computeAve() method to calculate the average grade and then checks if the gradeAve property is greater than or equal to 85. The result is stored in the this.passed property.

The willPassWithHonors() method determines if the student passed with honors based on their average grade. It first checks if the student passed (this.passed is true) and then checks if the gradeAve property is greater than or equal to 90. The result is stored in the this.passedWithHonors property.

let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);
*/