console.log("hello");


//1) What is the blueprint where objects are created from?
//Answer: class
//2) What is the naming convention applied to classes?
//Answer: pascal casing?
//3) What keyword do we use to create objects from a class?
//Answer: class
//4) What is the technical term for creating an object from a class?
//Answer: instantiation
//5) What class method dictates HOW objects will be created from that class?
//Answer: constructor

//Student Class
class Student {

	constructor(name, email, grades){
		this.name = name;
		this.email = email;
		this.grades = grades;
	};

	login(){
		console.log(`${this.email} has logged in`);
		return this;
	};
	logout(){
		console.log(`${this.email} has logged off`);
		return this;
	};
	listGrades() {
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		return this;
	};
	computeAve() {
		let sum = 0;
		this.grades.forEach(grades => sum += grades);
		this.gradesAverage = sum / 4;
		return this.gradesAverage;
	};
	willPass() {
		this.passed = this.computeAve().gradesAverage >= 85;
		return this.passed;
	};
	willPassWithHonors() {
		if (this.passed) {
			this.passedWithHonors = this.gradesAverage >= 90;
		} else {
			this.passedWithHonors = false;
		}
		return this.passedWithHonors;
	};
}

//Instances
let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
studentOne.login();
studentOne.logout();
studentOne.listGrades();
let s14 = studentOne.computeAve();
console.log(s14);
let s15 = studentOne.willPass();
console.log(s15);
let s16 = studentOne.willPassWithHonors();
console.log(s16);

let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
studentTwo.login();
studentTwo.logout();
studentTwo.listGrades();
let s24 = studentTwo.computeAve();
console.log(s24);
let s25 = studentTwo.willPass();
console.log(s25);
let s26 = studentTwo.willPassWithHonors();
console.log(s26);

let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
studentThree.login();
studentThree.logout();
studentThree.listGrades();
let s34 = studentThree.computeAve();
console.log(s34);
let s35 = studentThree.willPass();
console.log(s35);
let s36 = studentThree.willPassWithHonors();
console.log(s36);

let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);
studentFour.login();
studentFour.logout();
studentFour.listGrades();
let s44 = studentFour.computeAve();
console.log(s44);
let s45 = studentFour.willPass();
console.log(s45);
let s46 = studentFour.willPassWithHonors();
console.log(s46);